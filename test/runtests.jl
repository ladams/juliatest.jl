using JuliaTest
using Test

# write your own tests here
@testset "Does math still work???" begin
    @test 1 == 1
    @test 2 + 2 == 4
end

@testset "My stuff" begin
    @test JuliaTest.greet("Amanda") == "Hello my dear Amanda!"
    @test JuliaTest.greet("Luke") == "Hello Luke!"

    @test JuliaTest.my_add(1, 2) ≈ 3 atol=2
end