using Pkg, InteractiveUtils

InteractiveUtils.versioninfo()

@info "Building package" path=pwd()
Pkg.develop(PackageSpec(url=pwd()))
Pkg.build(ENV["PKG_NAME"])

@info "Instantiating docs enviroment"
Pkg.activate("docs")
Pkg.instantiate()

@info "Instantiating coverage enviroment"
Pkg.activate("ci/coverage")
Pkg.instantiate()