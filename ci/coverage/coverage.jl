using Coverage

cl, tl = get_summary(process_folder())
println("(", cl/tl*100, "%) covered")

Codecov.submit_local(process_folder(), pwd())