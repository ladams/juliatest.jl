module JuliaTest

using Random

"""
    greet(name)

Returns a string to great `name`. If `name` is Amanda, return an extra special
string.
"""
function greet(name)
	if name == "Amanda"
		return "Hello my dear $(name)!"
	else
		return "Hello $(name)!"
	end
end

"""
    my_add(a, b)

Returns the sum of `a` and `b`, plus some extra fun!
"""
my_add(a, b) = a + b + randn()

end # module
