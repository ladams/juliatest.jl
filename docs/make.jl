using Documenter, JuliaTest

makedocs(
    format=:html,
    sitename = "JuliaTest",
    repo = "https://gitlab.com/ladams/JuliaTest.jl/blob/{commit}{path}#{line}",
    pages = [
        "index.md"
    ]
)