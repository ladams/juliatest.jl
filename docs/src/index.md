# JuliaTest.jl Documentation

This is a test of generating documentation for the test package `JuliaTest`. There
are so many tests it is making me dizzy!

```@meta
CurrentModule = JuliaTest
```

```@docs
greet
my_add
```